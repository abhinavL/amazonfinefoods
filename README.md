## AmazonFoodReviewsCaseStudy
Case Study for Classification of Amazon Food Reviews

This repository contains a complete case study for Classification of Amazon Food Reviews Dataset. The Dataset can be downloaded from Kaggle: https://www.kaggle.com/snap/amazon-fine-food-reviews This case Study Contains Implementation of Following Algorithms

* GBDT
* Random Forest
* Logistic Regression
* Naive Bayes
* Hierarchical Clustering
* DBSCAN
* KMeans
* KNN
* Decision Tree

To run the project follow the steps given below:
 *  Install following:
  * * Anaconda 5.1.*
  * * python 3.6.*
  * * skitlearn
  * * numpy
  * * matplotlib
 * Download the data set from above specified URL and save it in same directory.